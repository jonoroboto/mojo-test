var express = require("express");
var router = express.Router();

router.get("/", function (req, res, next) {
  res.json([
    {
      uid: 100001,
      name: "Bauhaus Archive",
      address: "7246 Woodland Rd. Waukesha, WI 53186",
      users: 906,
      offices: 36,
      type: "Rent",
      price: 75,
      position: {
        lat: 52.95,
        lng: -1.24,
      },
    },
    {
      uid: 100002,
      name: "Lotus Temple",
      address: "164 S. Carson Court Newport News, VA 23601",
      users: 876,
      offices: 20,
      type: "Rent",
      price: 65,
      position: { lat: 52.96, lng: -1.25 },
    },
    {
      uid: 100003,
      name: "Dome of the Rock",
      address: "123 Johnson Court Roy, UT 84067",
      users: 564,
      offices: 46,
      type: "Rent",
      price: 96,
      position: { lat: 52.97, lng: -1.26 },
    },
    {
      uid: 100004,
      name: "Lloyd's Building",
      address: "36 Squaw Creek Dr. Harleysville, PA 19438",
      users: 466,
      offices: 120,
      type: "Rent",
      price: 120,
      position: { lat: 52.94, lng: -1.23 },
    },
    {
      uid: 100005,
      name: "Amityville house",
      address: "12 Amityville Road. Kentucky, PA 19438",
      users: 3,
      offices: 0,
      type: "Rent",
      price: 10,
      position: { lat: 52.94, lng: -1.24 },
    },
    {
      uid: 100006,
      name: "The Hollywood Roosevelt",
      address: "7000 Hollywood Blvd, Los Angeles, CA 90028, United States",
      users: 20140,
      offices: 28,
      type: "Rent",
      price: 200,
      position: { lat: 52.95, lng: -1.24 },
    },
    {
      uid: 100007,
      name: "Another building",
      address: "123 Nottingham Road, CA 90028, United States",
      users: 12,
      offices: 2,
      type: "Sale",
      price: 200,
      position: { lat: 52.91, lng: -1.21 },
    },
    {
      uid: 100008,
      name: "The Eiffel Tower",
      address: "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
      users: 124144,
      offices: 49,
      type: "Rent",
      price: 3000,
      position: { lat: 48.8583701, lng: 2.2944813 },
    },
  ]);
});

module.exports = router;
