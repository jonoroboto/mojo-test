# Get me started!

### `yarn start`

You will need to run this inside of both the root, and another instance inside of the API folder, with the clientside available on [localhost:3000](http://localhost:3000/) and the API available on [localhost:9000](http://localhost:9000/).

### More details

It's using styled components, react, node.js api, redux and it's fully responsive down to iPhone 6, with a little work can go further. Worth mentioning, check out the horizontal overflow on mobile, love this pattern.
