const theme = {
  color: {
    blue: "#1665d8",
    divider: "#2e2e33",
    dividerLight: "#eaedf3",
    input: "#e2e5ed",
    white: "#ffffff",
    dark: "#252529",
    light: "#9ea0a5",
    lighter: "#cecfd2",
  },
  base: "9px",
  nav: "270px",
  list: "600px",
  content: "900px",
  gutter: "9px",
  radius: "4px",
  breakpoints: {
    xs: "400px",
    sm: "600px",
    md: "900px",
    lg: "1200px",
    xl: "1600px",
  },
  opacity: {
    default: 0.2,
  },
};

export default theme;
