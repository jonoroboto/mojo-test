import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import { Counter } from "./features/counter/Counter";
import Dashboard from "./pages/dashboard";
import Help from "./pages/help";
import Elsewhere from "./pages/elsewhere";
import Notification from "./pages/notification";
import Test from "./pages/test-api";

import "./App.css";

import styled from "@emotion/styled";
import theme from "./utils/theme";

import Nav from "./components/navigation/nav";

const Grid = styled.div`
  display: grid;
  grid-template-columns: ${theme.nav} 1fr;
  height: 100vh;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-template-columns: 1fr;
    grid-auto-rows: auto 80px;
  }
`;

function App() {
  return (
    <div className="App">
      <Grid>
        <Router>
          <Nav />
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/help" component={Help} />
            <Route exact path="/elsewhere" component={Elsewhere} />
            <Route exact path="/notification" component={Notification} />
            <Route exact path="/test" component={Test} />
          </Switch>
        </Router>
      </Grid>
    </div>
  );
}

export default App;
