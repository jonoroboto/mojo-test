import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import fetchBuildingsAction from "../../redux/fetchBuildings";
import {
  getBuildingsError,
  getBuildings,
  getBuildingsPending,
} from "../../redux/reducer";

import styled from "@emotion/styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThList, faThLarge } from "@fortawesome/free-solid-svg-icons";

import Listing from "./listing";
import theme from "../../utils/theme";

const Container = styled.div`
  box-shadow: 1px 0 8px 0 rgba(0, 0, 0, 0.1), 1px 0 0 0 rgba(0, 0, 0, 0.06);
  @media (max-width: ${theme.breakpoints.lg}) {
    height: unset;
    overflow: auto;
  }
`;

const ListOuter = styled.div`
  /* I hate that it has to be done this way, but there's no easy way to create a simple overflow */
  height: calc(100vh - 150px);
  overflow: auto;
  @media (max-width: ${theme.breakpoints.lg}) {
    height: unset;
    overflow: auto;
  }
`;
const ListContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 30px;
  padding: 0 30px 30px 30px;
  box-sizing: border-box;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-template-columns: unset;
    display: flex;
    flex-wrap: nowrap;
    padding: 16px 0;
    flex: 0 0 auto;
    > * + * {
      margin-left: 32px;
    }
    &:before,
    &:after {
      content: "";
      display: block;
      padding: 0 15px;
    }
  }
`;
const SortTool = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  justify-content: space-between;
  padding: 0 30px;
  box-sizing: border-box;
  align-items: center;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;
const Title = styled.h2``;
const Layout = styled.div`
  color: ${theme.color.lighter};
  * + * {
    border-left: ${theme.color.lighter} solid 1px;
    padding-left: 8px;
    margin-left: 8px;
  }
`;

class List extends Component {
  componentDidMount() {
    const { fetchBuildings } = this.props;
    fetchBuildings();
  }

  render() {
    const { error, buildings } = this.props;

    return (
      <Container>
        <SortTool>
          <Title>{buildings.length} Buildings</Title>
          <Layout>
            <FontAwesomeIcon icon={faThList} />
            <FontAwesomeIcon icon={faThLarge} />
          </Layout>
        </SortTool>
        <ListOuter>
          <ListContainer>
            {error && <span className="product-list-error">{error}</span>}
            {buildings.map((b) => (
              <Listing key={b.uid} building={b} />
            ))}
          </ListContainer>
        </ListOuter>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  error: getBuildingsError(state),
  buildings: getBuildings(state),
  pending: getBuildingsPending(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchBuildings: fetchBuildingsAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(List);
