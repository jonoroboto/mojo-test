import React, { Component } from "react";
import theme from "../../utils/theme";
import styled from "@emotion/styled";
import Building from "../../dummy/building.jpg";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBuilding, faUser } from "@fortawesome/free-solid-svg-icons";

const Logo = styled(FontAwesomeIcon)`
  font-size: 18px;
  margin-right: 12px;
`;

const Card = styled.div`
  width: 100%;
  text-align: left;
  @media (max-width: ${theme.breakpoints.lg}) {
    width: 200px;
    flex-shrink: 0;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    width: 150px;
  }
`;
const Image = styled.img`
  width: 100%;
  border-radius: ${theme.radius};
`;
const Title = styled.h3`
  font-size: 16px;
  margin: 20px 0 5px 0;
  @media (max-width: ${theme.breakpoints.sm}) {
    margin: 8px 0 0 0;
  }
`;
const Address = styled.h4`
  color: ${theme.color.light};
  font-weight: 400;
  font-size: 14px;
  line-height: 1.57;
  margin: 0;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;
const Details = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 50%);
  margin: 0;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;
const Users = styled.h5`
  color: ${theme.color.light};
`;
const Offices = styled.h5`
  color: ${theme.color.light};
`;
const Rent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Type = styled.h3`
  color: ${theme.color.blue};
  font-size: 14px;
  font-weight: 400;
  @media (max-width: ${theme.breakpoints.md}) {
    font-size: 10px;
  }
`;
const Price = styled.h2`
  font-size: 18px;
  @media (max-width: ${theme.breakpoints.md}) {
    font-size: 12px;
  }
`;

export default class listing extends Component {
  render() {
    const { building } = this.props;
    console.log(building);
    return (
      <Card>
        <Image src={Building} />
        <Title>{building.title}</Title>
        <Address>{building.address}</Address>
        <Details>
          <Users>
            <Logo icon={faUser} /> {building.users} Users
          </Users>
          <Offices>
            <Logo icon={faBuilding} />
            {building.offices} Offices
          </Offices>
        </Details>
        <Rent>
          <Type>For rent</Type>
          <Price>£{building.price}/sqm</Price>
        </Rent>
      </Card>
    );
  }
}
