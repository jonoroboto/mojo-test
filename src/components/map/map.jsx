import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import fetchBuildingsAction from "../../redux/fetchBuildings";
import {
  getBuildingsError,
  getBuildings,
  getBuildingsPending,
} from "../../redux/reducer";

import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import styled from "@emotion/styled";

const Outer = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`;

const mapStyles = {
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
};

export class MapContainer extends Component {
  componentDidMount() {
    const { fetchBuildings } = this.props;
    fetchBuildings();
  }

  render() {
    const { error, buildings } = this.props;
    return (
      <Outer>
        {error && <span className="product-list-error">{error}</span>}
        <Map
          google={this.props.google}
          zoom={14}
          style={mapStyles}
          initialCenter={{
            lat: 52.9540019,
            lng: -1.2401012,
          }}
        >
          {buildings.map((b) => (
            <Marker key={b.uid} title={b.name} position={b.position} />
          ))}
        </Map>
      </Outer>
    );
  }
}

export const MapWrapper = GoogleApiWrapper({
  apiKey: "AIzaSyB8udj6JL47_A_El2yaKXctP4mHL3o59Tk",
})(MapContainer);

const mapStateToProps = (state) => ({
  error: getBuildingsError(state),
  buildings: getBuildings(state),
  pending: getBuildingsPending(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchBuildings: fetchBuildingsAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  MapWrapper,
  MapContainer
);
