import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faBuilding } from "@fortawesome/free-solid-svg-icons";

import styled from "@emotion/styled";
import theme from "../../utils/theme";

const NavContainer = styled.nav`
  grid-column: span 2;
  height: 100%;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.06);
  z-index: 1;
  background: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 30px;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-column: unset;
  }
`;
const RightPanel = styled.div`
  display: flex;
  align-items: center;
`;
const Divider = styled.hr`
  margin: 16px 30px;
  width: 1px;
  height: 30px;
  background: ${theme.color.dividerLight};
  border: 0;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;
const Logo = styled(FontAwesomeIcon)`
  @media (min-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;

// Search bar
const SearchBar = styled.div`
  font-size: 14px;
  box-sizing: border-box;
  border: 1px solid ${theme.color.input};
  border-radius: ${theme.radius};
  position: relative;
`;
const SearchIcon = styled(FontAwesomeIcon)`
  margin-right: 12px;
  color: ${theme.color.light};
  position: absolute;
  right: 12px;
  top: 0;
  bottom: 0;
  margin: auto;
`;
const SearchBarInput = styled.input`
  padding: 10px;
  font-size: 14px;
  font-family: "Roboto", sans-serif;
  box-sizing: border-box;
  border-radius: ${theme.radius};
  border: 0;
  width: 300px;
  &::placeholder {
    color: ${theme.color.light};
  }
  @media (max-width: ${theme.breakpoints.lg}) {
    width: 200px;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    width: 160px;
  }
`;
const BuildingSelect = styled.select`
  padding: 10px;
  font-size: 14px;
  font-weight: 500;
  font-family: "Roboto", sans-serif;
  box-sizing: border-box;
  border: 1px solid ${theme.color.input};
  border-radius: ${theme.radius};
  background-color: transparent;
  appearance: none;
`;
const CountrySelect = styled.select`
  padding: 10px;
  font-size: 14px;
  font-weight: 500;
  font-family: "Roboto", sans-serif;
  box-sizing: border-box;
  border: 1px solid ${theme.color.input};
  border-radius: ${theme.radius};
  background-color: transparent;
  appearance: none;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;

export default function StickyNav() {
  return (
    <NavContainer>
      <Logo icon={faBuilding} size="lg" />

      <BuildingSelect name="BuildingSelect" id="building_select">
        <option default value="all">
          All Buildings
        </option>
        <option value="flats">Flats</option>
        <option value="houses">Houses</option>
        <option value="semi-detached">Semi-Detached</option>
      </BuildingSelect>

      <RightPanel>
        <SearchBar>
          <SearchBarInput
            type="text"
            placeholder="Type to search..."
            id="search_bar"
          ></SearchBarInput>
          <label htmlFor="search_bar">
            <SearchIcon icon={faSearch} />
          </label>
        </SearchBar>
        <Divider />
        <CountrySelect name="BuildingSelect" id="building_select">
          <option default value="all">
            ENG
          </option>
          <option value="some">Some</option>
          <option value="other">Other</option>
          <option value="languages">Languages</option>
        </CountrySelect>
      </RightPanel>
    </NavContainer>
  );
}
