import React from "react";
import { NavLink } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBuilding,
  faHome,
  faBell,
  faQuestion,
} from "@fortawesome/free-solid-svg-icons";

import styled from "@emotion/styled";
import theme from "../../utils/theme";

const NavContainer = styled.nav`
  max-width: ${theme.nav};
  background: ${theme.color.dark};
  color: #fff;
  height: 100%;
  padding: 0;
  text-align: left;
  box-sizing: border-box;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-row: 2;
    max-width: unset;
    width: 100%;
  }
`;
const Logo = styled(FontAwesomeIcon)`
  margin-right: 16px;
  font-weight: 500;
`;

const LogoContainer = styled.div`
  width: 100%;
  height: 70px;
  padding: 0 39px;
  border-bottom: 1px solid ${theme.color.divider};
  box-sizing: border-box;
  display: flex;
  align-items: center;
  font-weight: 500;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;

const Subtitle = styled.h5`
  color: ${theme.color.light};
  padding: ${theme.base};
  text-transform: uppercase;
  padding-bottom: 19px;
  margin: 0;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;
const NavList = styled.div`
  padding: ${theme.base} 30px;
  padding-bottom: 19px;
  display: grid;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-template-columns: repeat(4, 1fr);
  }
`;

const NavItem = styled(NavLink)`
  padding: ${theme.base};
  border-radius: ${theme.radius};
  font-weight: 500;
  color: #fff;
  text-decoration: none;
  display: flex;
  justify-content: space-between;
  &.active {
    background: ${theme.color.blue};
  }
  span {
    width: 20px;
    height: 20px;
    border-radius: 20px;
    background: ${theme.color.white};
    text-align: center;
    font-size: 12px;
    line-height: 20px;
    color: ${theme.color.dark};
  }
  @media (max-width: ${theme.breakpoints.lg}) {
    display: ${(props) => (props.essential ? "flex" : "none")};
    flex-direction: column;
    align-items: center;
    text-align: center;
    font-weight: 400;
    font-size: 12px;
    span {
      display: none;
    }
    svg {
      margin-bottom: 8px;
    }
  }
  @media (max-width: ${theme.breakpoints.lg}) {
    font-size: 10px;
  }
`;
const NavIcon = styled(FontAwesomeIcon)`
  display: none;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: block;
  }
`;
const NavDivider = styled.hr`
  border: none;
  width: 100%;
  height: 1px;
  background: ${theme.color.divider};
  margin: 20px 0;
  @media (max-width: ${theme.breakpoints.lg}) {
    display: none;
  }
`;

export default function Nav() {
  return (
    <NavContainer>
      <LogoContainer>
        <Logo icon={faBuilding} />
        Buildings
      </LogoContainer>

      {/* Top nav */}
      <NavList>
        <Subtitle>Menu</Subtitle>
        <NavItem essential exact activeClassName="active" to="/">
          <NavIcon icon={faHome} />
          Dashboard
        </NavItem>
        <NavItem essential exact activeClassName="active" to="/elsewhere">
          <NavIcon icon={faBuilding} />
          Buildings
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Posts
        </NavItem>
        <NavItem essential exact activeClassName="active" to="/notification">
          <NavIcon icon={faBell} />
          Conversations <span>2</span>
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Amenities
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Tenants
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Performance
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Users
        </NavItem>

        <NavDivider />

        {/* Bottom Nav */}
        <Subtitle>Support</Subtitle>
        <NavItem essential exact activeClassName="active" to="/help">
          <NavIcon icon={faQuestion} />
          Need Help?
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Contact Us
        </NavItem>
        <NavItem activeClassName="active" to="/elsewhere">
          Knowledge Base
        </NavItem>
      </NavList>
    </NavContainer>
  );
}
