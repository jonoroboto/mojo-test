import React, { Component } from "react";
import styled from "@emotion/styled";
import theme from "../utils/theme";
import StickyNav from "../components/navigation/stickynav.jsx";
import PropertyListing from "../components/listing/list.jsx";
import Map from "../components/map/map.jsx";

const Grid = styled.div`
  display: grid;
  grid-template-rows: 70px 1fr;
  grid-template-columns: ${theme.list} 1fr;
  @media (max-width: ${theme.breakpoints.lg}) {
    grid-template-columns: 1fr;
    grid-template-rows: 70px auto 1fr;
  }
`;

export default class Dashboard extends Component {
  render() {
    return (
      <Grid>
        <StickyNav />
        <PropertyListing />
        <Map />
      </Grid>
    );
  }
}
