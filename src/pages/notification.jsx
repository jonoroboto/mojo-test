import React from "react";
import styled from "@emotion/styled";
import theme from "../utils/theme";

const Content = styled.div`
  max-width: ${theme.content};
  margin: 32px auto;
  padding: 0 32px;
`;

export default function Notifications() {
  return (
    <Content>
      <h1>Notifications</h1>
      <p>
        Thought I'd throw a highlight in with the notification, they're
        currently a simple span.
      </p>
    </Content>
  );
}
