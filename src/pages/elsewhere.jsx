import React from "react";
import BigKev from "../big_kev.jpg";
import styled from "@emotion/styled";
import theme from "../utils/theme";

const Content = styled.div`
  max-width: ${theme.content};
  margin: 32px auto;
  padding: 0 32px;
`;
const Kev = styled.img`
  max-width: 600px;
  width: 100%;
`;

export default function Elsewhere() {
  return (
    <Content>
      <h1>Welcome to elsewhere</h1>
      <p>
        I was going to consider adding individual divages for the navitems but I
        figured my time would be better spent elsewhere. So instead I added a
        picture of me with Big Kev
      </p>
      <Kev src={BigKev} alt="A picture of big kev" />
    </Content>
  );
}
