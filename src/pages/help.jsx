import React from "react";
import BigKev from "../big_kev_2.jpeg";
import styled from "@emotion/styled";
import theme from "../utils/theme";

const Content = styled.div`
  max-width: ${theme.content};
  margin: 32px auto;
  padding: 0 32px;
`;
const Kev = styled.img`
  max-width: 600px;
  width: 100%;
`;

export default function Help() {
  return (
    <Content>
      <h1>There's no help here</h1>
      <p>Just another picture of Big Kev</p>
      <Kev src={BigKev} alt="Another picture of big kev" />
    </Content>
  );
}
