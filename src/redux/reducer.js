import {
  FETCH_BUILDINGS_PENDING,
  FETCH_BUILDINGS_SUCCESS,
  FETCH_BUILDINGS_ERROR,
} from "./actions";

const initialState = {
  pending: false,
  buildings: [],
  error: null,
};

export function buildingsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BUILDINGS_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_BUILDINGS_SUCCESS:
      return {
        ...state,
        pending: false,
        buildings: action.payload,
      };
    case FETCH_BUILDINGS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getBuildings = (state) => state.buildings;
export const getBuildingsPending = (state) => state.pending;
export const getBuildingsError = (state) => state.error;
