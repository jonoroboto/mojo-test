import {
  fetchBuildingsPending,
  fetchBuildingsSuccess,
  fetchBuildingsError,
} from "./actions";

function fetchBuildings() {
  return (dispatch) => {
    dispatch(fetchBuildingsPending());
    fetch("http://localhost:9000/buildings")
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw res.error;
        }
        dispatch(fetchBuildingsSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(fetchBuildingsError(error));
      });
  };
}

export default fetchBuildings;
