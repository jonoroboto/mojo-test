export const FETCH_BUILDINGS_PENDING = "FETCH_BUILDINGS_PENDING";
export const FETCH_BUILDINGS_SUCCESS = "FETCH_BUILDINGS_SUCCESS";
export const FETCH_BUILDINGS_ERROR = "FETCH_BUILDINGS_ERROR";

export function fetchBuildingsPending() {
  return {
    type: FETCH_BUILDINGS_PENDING,
  };
}

export function fetchBuildingsSuccess(buildings) {
  return {
    type: FETCH_BUILDINGS_SUCCESS,
    payload: buildings,
  };
}

export function fetchBuildingsError(error) {
  return {
    type: FETCH_BUILDINGS_ERROR,
    error: error,
  };
}
