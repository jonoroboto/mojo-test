import { applyMiddleware, createStore, compose } from "redux";
import thunk from "redux-thunk";

import { buildingsReducer } from "./reducer.js";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  buildingsReducer,
  composeEnhancers(applyMiddleware(thunk))
);
